//
//  ViewController.swift
//  SwiftAndObjectiveCAssignment
//
//  Created by Douglas Sass on 1/27/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // 1- How do you create and assign a boolean
        let oneBoolean = false
        print(oneBoolean)
        
        // 2- How do you create and assign a String
        let twoString = "strings"
        print(twoString)
        
        // 3- How do you create and assign a integer
        var threeInteger = 3
        print(threeInteger)
        
        // 4- How do you create and assign a float
        let fourFloat: Float = 4.0
        print(fourFloat)
        
        // 5- How do you create and assign a double
        let fiveDouble: Double = 5.0
        print(fiveDouble)
        
        // 6- How do you create an if else statement
        if oneBoolean {
            print("One is true")
        } else {
            print("One is false")
        }
        
        // 7 - How do you create and initialize an array with 3 variables
        var sevenArray = ["Hello", "this", "world"]
        print(sevenArray)
        
        // 8 - How do you add a variable to an array after it has been created
        sevenArray.insert("great", at: 2)
        print(sevenArray)
        
        // 9- How do you create a dictionary with 3 key value pairs
        var nineDictionary = [ 1 : "Hello", 2 : "new", 3 : "computer"]
        print(nineDictionary)
        
        // 10 - How do you add a new key value pair to a dictionary after it has been created
        nineDictionary[4] = "!"
        print(nineDictionary)
        
        // 11 - How do you create a for loop
        for i in sevenArray {
            print(i, terminator:" ")
        }
            
        // 12 - How do you create a while loop
        while threeInteger < 8 {
            print(threeInteger, terminator:" ")
            threeInteger += 1
        }
        
        // 13 - How do you create a function
        func hello(to: String) {
            let helloName = "Hello \(to), you are great!"
            print(helloName)
        }
        
        // 14 - How would you call the function you created in #13
        hello(to: "Kayla")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

